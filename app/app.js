
var app = angular.module( "projectManagement", ["ngRoute","ngSanitize","ui.bootstrap"] );
var baseUrl = "http://localhost/project-management/server/index.php/";

app.config(function($routeProvider){

	var authentication = function( server ){
		return server.checkLoginStatus();
	}

	$routeProvider
		.when("/", {
			templateUrl : "app/pages/login.html",
			controller  : "loginCtrl",
			resolve     : { "check": function( server ){
				
				server.checkLoginStatus( 'loginCtrl' );

			}}
		})
		.when("/dashboard",{
			templateUrl : "app/pages/dashboard.html",
			controller  : "dashboardCtrl",
			resolve     : { "check": authentication }
		})
		.when("/user",{
			templateUrl : "app/pages/users.html",
			controller  : "userCtrl",
			resolve     : { "check": authentication } 
		})
		.when("/user/:action",{
			templateUrl : "app/pages/users.html",
			controller  : "userCtrl",
			resolve     : { "check": authentication } 
		})
		.when("/user.roles",{
			templateUrl : "app/pages/user-roles.html",
			controller  : "userRolesCtrl",
			resolve     : { "check": authentication } 
		})
		.when("/error",{
			templateUrl : "app/pages/error.html",
			controller  : "errorCtrl" 
		})
		.otherwise({redirectTo : '/'});

});

app.run( function(  $http, server ) {

		
    }
  );
