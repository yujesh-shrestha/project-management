(function(){
	
	app.factory( 'utils',function( $filter ){

		var pop = function( data,ID ){
			return  data.filter(function(el){ return el.ID != ID });
		}

		var setTitle = function( title ){
			document.querySelector('title').innerHTML = title;
		}
		
		var toast = function( msg )
		{
			Materialize.toast(msg, 2000, 'square');
		}

		var filter = function( searchIn, searchBy ){
			return ($filter('filter')( searchIn, searchBy ))[0];
		}

		return {
			pop 	 : pop,
			setTitle : setTitle,
			toast    : toast,
			filter   : filter
			 
		}
	});

}());