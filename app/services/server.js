(function(){

	app.factory( 'server', function( $rootScope, $http, $location, $log, buffer ){

		var getUser =  function( ID ){

			var url = baseUrl + "user/get/";
			if( typeof ID !== 'undefined' ){
				url += ID;
			}

			return $http.get( url ).then(function(response){
				return response.data;
			});
		}

		var deleteUser = function( ID ){
			return $http.get(baseUrl+"user/delete/"+ID).then(function(response){
				return response.data;
			});
		}

		var saveUser = function( data ){
			return $http.post(baseUrl + "user/save",data).then(function(response){
				return response.data;
			});
		}

		var getRole = function( ID ){

			var url = baseUrl + "userrole/get/";
			if( typeof ID !== 'undefined' ){
				url += ID;
			}
			return $http.get( url ).then(function( response ){
				return response.data;
			});
		}

		var saveRole = function( data ){
			return $http.post(baseUrl + "userrole/save",data).then(function(response){
				return response.data;
			});
		}

		var deleteRole = function( ID ){
			return $http.get(baseUrl+"userrole/delete/"+ID).then(function(response){
				return response.data;
			});
		}

		var login = function( user ){
			return $http.post( baseUrl + 'user/login',user ).then( function( response ){
				return response.data;
			} );
		}

		var logout = function(){
			return $http.post( baseUrl + 'user/logout' ).then( function(response){
				return response.data;
			});
		}

		var checkLoginStatus = function( ctrl ){

			var onResponse = function( response ){

				if( response.data.status == true ){

					$rootScope.user = response.data.data;
					$rootScope.loggedIn = true;

					$log.info( 'User is Logged In');

					if( ctrl == 'loginCtrl' ){
						$location.path( '/dashboard' );
					}


				}else{

					$location.path( '/' );
					$log.info( 'User is not Logged In');

				}

			}

			var onError = function( reason ){
				
				if( reason.status == 500 ){
					$log.error( reason.data );
					buffer.error1 = reason.statusText;
					buffer.error2 = "Please Check your Database connection.";
					$location.url( '/error');
				}
			}

			if( !$rootScope.loggedIn ){

				$log.info("checkLoginStatus..");

				if( typeof ctrl == 'undefined' ){
					ctrl = false;
				}

				$http.get( baseUrl + 'user/checkLoginStatus' ).then( onResponse, onError ); 
			}
		}

		var getMenu = function(){
			return $http.get( baseUrl + 'userrole/getMenu' ).then( function( response ){
				
				return response.data;
			} );
		}

		return {
			getMenu    : getMenu,
			getUser    : getUser,
			saveUser   : saveUser, 
			deleteUser : deleteUser,
			getRole    : getRole,
			saveRole   : saveRole,
			deleteRole : deleteRole,
			login      : login,
			logout     : logout,
			checkLoginStatus : checkLoginStatus 
		};

	});

}());