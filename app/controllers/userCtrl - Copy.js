app.controller( "userCtrl",function( $scope, $rootScope, $routeParams, $location, server, utils, buffer ) {

	utils.setTitle( 'User | Project Management' );
	$scope.page = "User";
	$scope.submitBtn = 'Save';
	$scope.heading   = 'User';
	$scope.gotError = false;
	
	var action = $routeParams.action;

	var onError = function( reason ){
		$scope.gotError = true;

		if( reason.status == 500 ){
			$scope.error = reason.statusText;
		}
		else{
			$scope.error = reason.data;
		}	
	}
	
	var onGetUser = function(response){
		
		$scope.users = response;
	};

	var onGetRole = function( response ){
		$scope.roles = response;
	}

	var onDelete = function( response ){
		var msg = '';
		if( 'success' == response.status){
			msg = 'Deleted Successfully.';
			$scope.users = utils.pop( $scope.users,response.ID );
		}else{
			msg = 'Error On Delete.';
		}

		utils.toast(msg);
	}

	var onSave = function( response ){

		var msg = '';

		if( response.action == 'save' ){
			//$scope.users.push(response.data);
			$scope.users.splice(0,0,response.data);
			
			 msg = "Saved Successfully.";
		}
		else{
			msg = "Updated Successfully.";
		}
		
		$scope.editedUser = {};
		$scope.submitBtn = 'Save';
		utils.toast(msg);

	}

	var onLogout = function( response )
	{
		$rootScope.loggedIn = false;
		$location.url('/');
	}


	if( action == "logout" ){
		server.logout().then( onLogout, onError );
	}

	server.getUser().then( onGetUser, onError );
	server.getRole().then( onGetRole, onError );

	$scope.editedUser = {first_name:"",last_name:"",ID:"",user_role_id:"",password:""};

	$scope.edit =  function( user ){

		$scope.current = user;

		$scope.editedUser.username = user.username;
		$scope.editedUser.first_name = user.first_name;
		$scope.editedUser.last_name = user.last_name;
		$scope.editedUser.ID = user.ID;
		$scope.editedUser.password = user.password;
		$scope.editedUser.user_role_id = user.user_role_id;
		$scope.submitBtn ="Update";
	}

	$scope.delete = function( user ){

		if(utils.ask("Are you sure to delelte!")){
			server.deleteUser( user.ID ).then( onDelete,onError );
		}
	}

	$scope.submitForm = function(u){
		
		server.saveUser( u ).then( onSave,onError );

		$scope.current.username = u.username;
		$scope.current.first_name = u.first_name;
		$scope.current.last_name = u.last_name;
		$scope.current.password = u.password;
		
		server.getRole(u.user_role_id).then(function(response){
			$scope.current.role = response.name;
		});
		
	}
});