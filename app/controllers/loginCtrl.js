app.controller( 'loginCtrl',function( $scope, $rootScope, $location, $log, server, utils, buffer ){

	utils.setTitle( 'Login | Project Management' );

	var onLogin = function( response ){

		if( response.status == true ){
			$rootScope.user = response.data;
			$rootScope.loggedIn = true;
			$location.url('/dashboard');

		}else{

			utils.toast( "Incorrect Username or Password." );

		}
	}

	var onError = function( reason ){

		buffer.error1 = reason.statusText;
		buffer.error2 = "We cannot log you in currently.";
		$location.url( '/error' );

	}

	$scope.submit = function( user ){

		server.login( user ).then( onLogin, onError );
	}

});