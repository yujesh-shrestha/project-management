app.controller( "userCtrl",function( $uibModal, $scope, $rootScope, $routeParams, $location, server, utils ) {

	utils.setTitle( 'User | Project Management' );

	$scope.page = "User";
	$scope.addBtn = true;

	var action = $routeParams.action;

	var onError = function( reason ){
		utils.toast( reason.statusText );
	}
	
	var onGetUser = function(response){
		$scope.users = response;
	};

	var onGetRole = function( response ){
		$scope.roles = response;
	}

	var onDelete = function( response ){
		var msg = '';
		if( 'success' == response.status){
			msg = 'Deleted Successfully.';
			$scope.users = utils.pop( $scope.users,response.ID );
		}else{
			msg = 'Error On Delete.';
		}

		utils.toast(msg);
	}

	var onLogout = function( response )
	{
		$rootScope.loggedIn = false;
		$location.url('/');
	}

	if( action == "logout" ){
		server.logout().then( onLogout, onError );
	}

	server.getUser().then( onGetUser, onError );
	server.getRole().then( onGetRole, onError );

	var openModal = function(param){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'userForm.html',
	      controller: 'userFormCtrl',
	      size: 'lg',
	      resolve:  {
	      	items : function(){
	      		return param;
	      	}
	      }
	    });
	}

	$scope.open = function () { 
		var param = {
	      			action : "save",
	      			users : $scope.users,
	      		 	roles : $scope.roles
	      		 };

	     openModal( param ); 		 
 	};

	$scope.edit =  function( user ){

		$scope.editedUser = {
			"username" : user.username,
			"first_name" : user.first_name,
			"last_name" : user.last_name,
			"ID" : user.ID,
			"user_role_id" : user.user_role_id,
			"password" : user.password
		};

		var param = {
	      			action  : "update",
	      			current : user,
	      			editedUser : $scope.editedUser, 
	      			users : $scope.users,
	      		 	roles : $scope.roles
	      		 };

	    openModal( param );  		 
	}

	$scope.delete = function( user ){

		if(confirm("Are you sure to delelte!")){
			server.deleteUser( user.ID ).then( onDelete,onError );
		}
	}	
});

app.controller( "userFormCtrl", function( $scope, $uibModalInstance, utils, server, items ){

	$scope.submitBtn = items.action;
	$scope.users = items.users;
	$scope.roles = items.roles;

	if( items.action == "update" ){
		$scope.editedUser = items.editedUser;
		$scope.current = items.current;
	}

	var onError = function( reason ){
		utils.toast(reason.statusText);	
	}

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	  };

	$scope.submitForm = function(u){
		
		server.saveUser( u ).then( function( response ){

			var msg = '';

			if( response.action == 'save' ){
				$scope.users.splice(0,0,response.data);
				 msg = "Saved Successfully.";
			}
			else{

				msg = "Updated Successfully.";
				$scope.current.username = u.username;
				$scope.current.first_name = u.first_name;
				$scope.current.last_name = u.last_name;
				$scope.current.password = u.password;
				$scope.current.role = utils.filter($scope.roles, { ID:u.user_role_id } ).name;
			}
			
			$scope.editedUser = {};
			$scope.submitBtn = 'Save';
			utils.toast(msg);

	},onError );

	}

});