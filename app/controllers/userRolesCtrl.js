app.controller( 'userRolesCtrl',function( $scope, $uibModal, server, utils ){
	
	utils.setTitle( 'User Roles | Project Management' );
	$scope.page  = 'User Roles';
	$scope.addBtn = true;

	var onError = function( reason ){
		utils.toast( reason.data );
	}


	var onGetRole = function( response ){
		$scope.roles = response;
	}

	server.getRole().then( onGetRole,onError );

	var onDelete = function( response ){
		var msg = "Deleted Successfully.";
		$scope.roles = utils.pop( $scope.roles,response.ID );
		utils.toast(msg);
	}

	$scope.delete = function( role ){
		server.deleteRole( role.ID ).then( onDelete,onError );
	}

	var openModal = function(param){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'userRoleForm.html',
	      controller: 'userRoleFormCtrl',
	      size: 'lg',
	      resolve:  {
	      	items : function(){
	      		return param;
	      	}
	      }
	    });
	}

	$scope.edit = function( role ){

		$scope.editedRole = {
			"ID" : role.ID,
			"name" : role.name
		}

		var param = {
			action : "update",
			current : role,
			editedRole : $scope.editedRole
		};

		openModal( param );
	}

	$scope.open = function(){

		var param = {
			action : 'save',
			roles : $scope.roles
		};

		openModal( param );
	}
});

app.controller( "userRoleFormCtrl", function( $scope, $uibModalInstance, utils, server, items ){

	$scope.submitBtn = items.action;

	var onError = function( reason ){
		utils.toast( reason.data );
	}

	if( items.action == "save" ){
		$scope.roles = items.roles;
	}

	if( items.action == "update" ){
	
		$scope.current = items.current;
		$scope.editedRole = items.editedRole;
 
	}

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};

	$scope.submitForm = function( role ){

		server.saveRole( role ).then( function(response){
			var msg = '';

			if( response.action == 'save' ){

				$scope.roles.splice(0,0,response.data);
				 msg = "Saved Successfully.";

			}
			else{

				$scope.current.name = role.name;
				msg = "Updated Successfully.";

			}

			$scope.submitBtn = "Save";
			$scope.editedRole = {};

			utils.toast(msg);
		}, onError );
		
	}

});