<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	protected $neededColumn = 'u.username,u.password,u.first_name,u.last_name,u.user_role_id,u.created_on,u.ID,ur.name as role';

	public function login(){

		$data = array( 'action'=>'login' );
		if( $this->session->userdata( 'user_id' ) > 0 ){

			$id = $this->session->userdata( 'user_id' );
			$user = $this->user_m->get( $this->neededColumn,array('u.ID'=>$id),1);
			$data['data'] = $user;
			$data['message'] = 'Already Logged In.';
			$data['status'] = true;

		}else{

			$_POST = json_decode(file_get_contents('php://input'), true);

			$username = $this->input->post( 'username' );
			$password = $this->input->post( 'password' );
			$user = $this->user_m->get( $this->neededColumn, array( 'u.username'=>$username,'u.password'=>$password ),1 );
			
			if( $user ){
				$data['data'] = $user;
				$data['message'] = 'Logged In Successfully';
				$data['status'] = true;

				$sess_data = array( 
					'user_id'=>$user->ID,
					'username'=>$user->username,
					'role' => $user->role,
					'role_id'=>$user->user_role_id 
					);
				$this->session->set_userdata($sess_data);
			}else{
				$data['data'] = null;
				$data['message'] = 'Incorrect Username or password.';
				$data['status'] = false;
			}
		}

		echo json_encode($data);
	}

	public function logout(){
		$this->session->sess_destroy();
		$data = array(
			"message"=>"Logged Out Successfully.",
			"status"=>true,
			"action"=>"logout",
			"data"=>null
			);
	}

	public function get( $id = FALSE)
	{
		if( $this->isLoggedIn() ){
			if( $id == FALSE ){
				$users = $this->user_m->get( $this->neededColumn );
				if($users){
					$us = $users->result();
					 echo json_encode($us);
				}
				
				}else{
					$user = $this->user_m->get( $this->neededColumn,array('u.ID'=>$id),1 );
					echo json_encode($user);
				}
		}
		

	}

	public function save()
	{
		if($this->isLoggedIn()){
		$_POST = json_decode(file_get_contents('php://input'), true);
		
		$username =  $this->input->post('username');
		$first_name =  $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$password  = $this->input->post('password');
		$role_id   = $this->input->post('user_role_id');

		$id = $this->input->post('ID');
		$data = array( "username"=>$username,"first_name"=>$first_name, "last_name"=>$last_name,"password"=>$password,'user_role_id'=>$role_id );

		if(empty($id)){
			//save
			$id = $this->user_m->save($data);
			$data = array( 
				'status'=>true,
				'message'=>'updated',
				'action'=> 'save',
				'data'	=> $this->user_m->get( $this->neededColumn,array( 'u.ID'=>$id ),1 )
			);
		}
		else{
			//update
			$this->user_m->save($data,array("ID"=>$id));
			$data = array( 
				'status'=>true, 
				'message'=>'updated',
				'action'=> 'update', 
				'data'	=> $this->user_m->get( $this->neededColumn,array( 'u.ID'=>$id ),1 ) );
		}
		
		echo json_encode($data);
		}
	}

	public function delete( $id )
	{
		if( $this->isLoggedIn() ){
		$affected = $this->user_m->delete( array('ID'=>$id) );

		if($affected)
		{
			$response = array("status"=>"success","message"=>"Deleted Successfully.","ID"=>$id);
		}
		else
		{
			$response = array("status"=>"error","message"=>"Error Occured.");
		}
		
		echo json_encode($response);
	}
	}

	public function checkLoginStatus(){

		$data = array(
			'action' => 'loginCheck',
			'data' => null,
			'message'=>null
			);
		
		if( $this->session->userdata('user_id') > 0 ){
			$data['status'] = true;
			$data['data'] = array(
				"username" => $this->session->userdata( "username" ),
				"role" => $this->session->userdata( "role" ),
				"user_id" => $this->session->userdata( "user_id" ),
				"role_id" => $this->session->userdata( "role_id" )
				);
			$data['message'] = $this->session->userdata('user_id');
		}else{
			$data['status'] = false;
		}

		echo json_encode($data);
	}

	public function isLoggedIn(){

		if( $this->session->userdata('user_id') > 0 ){
			return true;
		}

		return false;
	}
}
