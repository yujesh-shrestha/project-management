<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userrole extends MY_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model( 'userrole_m' );
	}

	public function get( $id = FALSE)
	{
		if( $id == FALSE ){
			$users = $this->userrole_m->get();
			if($users)
			{
				$us = $users->result();
				 echo json_encode($us);
			}
			
		}else{
			$user = $this->userrole_m->get( '*',array('ID'=>$id),1 );
			echo json_encode($user);
		}

	}

	public function save()
	{
		$_POST = json_decode(file_get_contents('php://input'), true);
		
		$name =  $this->input->post('name');
		
		$id = $this->input->post('ID');
		$data = array( "name"=>$name );

		if(empty($id)){
			//save
			$id = $this->userrole_m->save($data);
			$data = array( 
				'status'=>true,
				'message'=>'updated',
				'action'=> 'save',
				'data'	=> $this->userrole_m->get( '*',array( 'ID'=>$id ),1 )
			);
		}
		else{
			//update
			$this->userrole_m->save($data,array("ID"=>$id));
			$data = array( 'status'=>true, 'message'=>'updated','action'=> 'update', 'data' => null );
		}
		
		echo json_encode($data);
		
	}

	public function delete( $id )
	{
		$affected = $this->userrole_m->delete( array('ID'=>$id) );

		if($affected)
		{
			$response = array("status"=>"success","message"=>"Deleted Successfully.","ID"=>$id);
		}
		else
		{
			$response = array("status"=>"error","message"=>"Error Occured.");
		}
		
		echo json_encode($response);
	}
}
