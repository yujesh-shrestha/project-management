<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    
    if( $this->session->userdata('user_id') > 0 ){
    	
    	return;
    }else{
    	echo json_encode(array(
    		"status"=>false,
    		"message"=>"Not Logged in.",
    		"data"=>null,
    		"action"=>null
    		));
    	exit();
    }
  }

  public function getMenu(){
   
    $menu = array();
    $this->makeMenu( 0,$menu );
     
     if( count($menu) > 0 ){
        $data = array(
        'status' => true,
        'data' => $menu,
        'action' => 'get_menu',
        'message' => 'Menu fetched successfully'
        );
      }else{
        $data = array(
          'status' => false,
          'data' => null,
          'action' => 'get_menu',
          'message' => 'Menus not found.'
          );
      }
    
    echo json_encode($data);
  }

  public function makeMenu( $parent_id, &$store ){

    $query = $this->menu_m->get( '*',array('parent_id'=>$parent_id) );

    if( $query ){
      $result = $query->result_array();
      foreach( $result as $key=>$row ){
         $store[$key] = $row;
         $this->makeMenu( $row['ID'],$store[$key]['children'] );
      }
    }

  }
}
