<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_m extends MY_Model {

	protected $table = "menu";
	protected $order = "DESC";
	protected $orderBy = "ID";
}