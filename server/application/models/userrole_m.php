<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userrole_m extends MY_Model {

	protected $table = "user_roles";
	protected $order = "DESC";
	protected $orderBy = "ID";
}